# Git Summary <img src="https://cdn.iconscout.com/icon/free/png-256/social-145-95506.png" alt="drawing" height="30" width="30"/>

## What is **Git** ?  

<a name="git"></a>Git is a **"version control system"**. It tells you **what** files changed, **who** changed them and **why** they changed it.

[GitHub](https://github.com/S1ddharth-Sharma), [Gitlab](https://www.gitlab.com/S1ddharth-Sharma), BitBucket etc, are websites for hosting projects that use **git**.

![alt](https://camo.githubusercontent.com/1d528ec2918ccfab80324bae5637bd7172f2ba92/68747470733a2f2f692e696d6775722e636f6d2f6334736b4e54752e706e673f31)

## Why use git ?

Suppose you’re working on a group **assignment** with some friends, and everyone has their own brilliant ideas. Consider two ways to combine these ideas together:

- Person A writes his/her/their ideas on a single sheet of paper and passes it on to Person B, who writes his/her/their ideas on the paper and passes it on to Person C, etc.
- Everyone writes their ideas down on separate pieces of paper, and then consolidates them into a single sheet afterward.

Clearly the **second option** is more efficient (and less annoying). That's why we use **[Git](#git)**. **[Git](#git)** allows each developer to work independently!

![alt](https://imgs.xkcd.com/comics/git_2x.png)



## Terminology


- **<a name="commit"></a>Commit** — stores the current contents of the index in a new **commit** along with a log message from the user describing the changes

- **<a name="branch"></a>Branch** — a pointer to a commit

- **<a name="master"></a>Master** — the default name for the first branch

- **Merge** — joining two or more commit histories

- **Workspace** — A name for your local copy of a Git repository

- **Working tree** — the current branch in your workspace; you see this in git status output all the time

- **Tracked and untracked files** — files either in the index cache or not yet added to it

- **Local repository** — another term for where you keep your copy of a Git repository on your workstation

- **Remote repository** — a secondary copy of a Git repository where you push changes for collaboration or backup

- **Push** - syncing your commits.

- **<a name="clone"></a>Clone** - making a copy of main code into local machine.

- **Fork** - copy repository with your username.

- **'origin/master'** — the default setting for a remote repository and its primary branch


![alt](https://softcover.s3.amazonaws.com/636/learn_enough_git/images/figures/git_status_sequence.png)

## Git Workflow


1) **Clone repo** -
   - $ git [clone](#clone) (link to repository)

**OR**
1) **Create a new repo** - 
   - $ git init #create an empty repository
2) **Create new [branch](#branch)** - 
   - $ git checkout [master](#master)
   - $ git checkout -b (your branch name)
3) **Staging changes** -
   - $ git add .  # To add untracked files
4) **[Commit](#commit) changes** -
   - $ git commit -sv # Description about the commit
5) **Push Files** -
   - $ git push origin (branch name)  # push changes into repository

![alt](https://worknme.files.wordpress.com/2016/07/git_areas.png)

