# Docker Summary <img src="https://cdn.iconscout.com/icon/free/png-256/social-275-116309.png" alt="drawing" height="30" width="30"/>

## What is Docker ?

Docker is an open source project that makes it easy to create containers and container-based apps. Originally built for Linux, Docker now runs on Windows and MacOS as well.

![alt](https://wiki.aquasec.com/download/attachments/2854889/Docker_Architecture.png?version=1&modificationDate=1520172700553&api=v2)

## Why Docker ?

![alt](https://pitstop.manageengine.com/support/ImageDisplay?downloadType=uploadedFile&fileName=1586956811819.png&blockId=6b6ce70e6c55f4e3253764dd2676dff0230bd882a70d1493&zgId=c065ea7ed2255947&mode=view)

Imagine you are working on an analysis in R and you send your code to a friend. Your friend runs exactly this code on exactly the same data set but gets a slightly different result. This can have various reasons such as a different operating system, a different version of an R package, et cetera. Docker is trying to solve problems like that.

A Docker container can be seen as a computer inside your computer. The cool thing about this virtual computer is that you can send it to your friends; And when they start this computer and run your code they will get exactly the same results as you did.

![alt](https://zdnet2.cbsistatic.com/hub/i/r/2017/05/08/af178c5a-64dd-4900-8447-3abd739757e3/resize/770xauto/78abd09a8d41c182a28118ac0465c914/docker-vm-container.png)

## Docker Terminologies

### 1. Docker
- Docker is a program used to develop, and run applications with containers.
  
### 2. Docker Image
- An image serves as the blueprint for creating containers that will host a certain application or service. Images are designed to be immutable; when you want to update what is inside your containers, you build a new image, rather than update your existing images.

### 3. Docker Container
- A Docker container is a running Docker image.
- From one image you can create multiple containers .

### Docker Hub
- Docker Hub (Like GitHub) is a cloud-based registry service which allows you to link to code repositories, build your images and test them, stores manually pushed images, and links to Docker Cloud so you can deploy images to your hosts.

![alt](https://www.systemconf.com/wp-content/uploads/2020/06/img_5ef9bc473881e.png)

## Docker Installation

![alt](/extras/docker.png)

Learn to Install docker on to your Ubuntu 16.04.

Uninstall the older version of docker if is already installed

    $ sudo apt-get remove docker docker-engine docker.io containerd runc

Installing CE (Community Docker Engine)

    $ sudo apt-get update
    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo apt-key fingerprint 0EBFCD88
    $ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable nightly test"
    $ sudo apt-get update
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

    // Check if docker is successfully installed in your system
    $ sudo docker run hello-world


## Docker Basic Commands

- **docker run** – Runs a command in a new container.
- **docker start** – Starts one or more stopped containers
- **docker stop** – Stops one or more running containers
- **docker build** – Builds an image form a Docker file
- **docker pull** – Pulls an image or a repository from a registry
- **docker push** – Pushes an image or a repository to a registry
- **docker export** – Exports a container’s filesystem as a tar archive
- **docker exec** – Runs a command in a run-time container
- **docker search** – Searches the Docker Hub for images
- **docker attach** – Attaches to a running container
- **docker commit** – Creates a new image from a container’s changes

![commands](https://raw.githubusercontent.com/philipz/docker_practice/master/_images/cmd_logic.png)]


## Docker Example

1. Download/pull the docker images that you want to work with.
2. Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.


**Example**

1. **Download the docker.**

        docker pull username/trydock

2. **Run the docker image with this command.**
    
        docker run -ti username/trydock /bin/bash

    Which will output like this, this means that now you are running the docker container and you are inside the container

        root@e0b72ff850f8:/#

    Every running container has an ID in your system. Container ID in my case is e0b72ff850f8.

3. **Let copy our code inside docker with this command.** (Make sure you are not inside docker terminal, enter exit in command line to get out of container terminal)

   - In this case, lets copy a simple python program which just prints "hello, I am talking from container". So lets consider you have this file hello.py

            print("hello, I am talking from container")

   - Copy file inside the docker container

            docker cp hello.py e0b72ff850f8:/

    where e0b72ff850f8 is the containerID. This will copy hello.py inside docker in root directory.

   - All write a script for installing dependencies - requirements.sh

            apt update
            apt install python3

   - Copy file inside docker

            docker cp requirements.sh e0b72ff850f8:/

4. **Install dependencies** If you want to install additional dependencies then you can, write all the steps for installation in a shell script and run the script inside the container.

   - Give permission to run shell script

            docker exec -it e0b72ff850f8 chmod +x requirements.sh

   - Install dependencies

            docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh

5. **Run the program inside container with this command.**
    
        docker start e0b72ff850f8
        docker exec e0b72ff850f8 python3 hello.py

    where **e0b72ff850f8** is the containerID.

6. **Save your copied program inside docker image with docker commit.**
    
        docker commit e0b72ff850f8 username/trydock

    where **e0b72ff850f8** is the containerID.

7. **Push docker image to the dockerhub.**

   - Tag image name with different name
    
            docker tag username/trydock another_username/repo

    where username is your username on dockerhub and repo is the name of image.
    For example:
    
            docker tag username/trydock yh42/trial-dock-img

   - Push on dockerhub

            docker push username/repo